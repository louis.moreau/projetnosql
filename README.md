# Projet NOSQL - ENSIIE MemeBoard
## Lancer le projet 
- Récupérez le projet sur git
- (Temporaire) supprimez les fichiers .gitkeep dans /website/data/sql et /website/data/mongodb
- (Temporaire) faites :
```
sudo chmod 777 -R .
```
- Lancez les dockers avec :
```
docker-compose up
```
- Connectez vous avec votre navigateur sur [localhost:8000](localhost:8000)

## Informations

 Le projet utilise 4 dockers :
- nginx
- php
- postgresSQL
- mongoDB

La partie avec mongoDB était prévue pour le stockage des images. Malheureusement, cette partie n'est pas fonctionnelle. Le stockage des images est donc fait en local.
