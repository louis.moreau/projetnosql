CREATE TABLE "publication" (
	"idpublication" SERIAL,
	"titrepublication" VARCHAR(250) NOT NULL,
	"typefichierpublication" VARCHAR(10) NOT NULL,
	"heurepublication" TIMESTAMP NOT NULL,
	CONSTRAINT "publication_pk" PRIMARY KEY ("idpublication")
) WITH (
  OIDS=FALSE
);





